var CustomEvent = require('./custom_event_polyfill');
var utils = require('./utils');

var DropDown = function(list, trigger) {
  this.hidden = true;
  this.list = list;
  this.trigger = trigger;
  this.items = [];
  this.getItems();
  this.addEvents();
  this.initialState = list.innerHTML;
};

Object.assign(DropDown.prototype, {
  getItems: function() {
    this.items = [].slice.call(this.list.querySelectorAll('li'));
    return this.items;
  },

  addEvents: function() {
    var self = this;
    // event delegation.
    this.list.addEventListener('click', function(e) {
      if(e.target.tagName === 'A' || e.target.tagName === 'button') {
        e.preventDefault();
        self.hide();
        var listEvent = new CustomEvent('click.dl', {
          detail: {
            list: self,
            selected: e.target,
            data: e.target.dataset,
          },
        });
        self.list.dispatchEvent(listEvent);
      }
    });
  },

  toggle: function() {
    if(this.hidden) {
      this.show();
    } else {
      this.hide();
    }
  },

  setData: function(data) {
    this.data = data;
    this.render(data);
  },

  addData: function(data) {
    this.data = (this.data || []).concat(data);
    this.render(data);
  },

  // call render manually on data;
  render: function(data){
    // empty the list first
    var sampleItem;
    var newChildren = [];
    var toAppend;

    this.items.forEach(function(item) {
      sampleItem = item;
      if(item.parentNode && item.parentNode.dataset.hasOwnProperty('dynamic')) {
        item.parentNode.removeChild(item);
      }
    });

    //changelog: changing map to Foreach as we only want filtered items
    this.data.forEach(function(dat){
      var html = utils.t(sampleItem.outerHTML, dat);
      var template = document.createElement('template');
      template.innerHTML = html;

      // Help set the image src template
      var imageTags = template.content.querySelectorAll('img[data-src]');
      for(var i = 0; i < imageTags.length; i++) {
        var imageTag = imageTags[i];
        imageTag.src = imageTag.getAttribute('data-src');
        imageTag.removeAttribute('data-src');
      }

      //changelog: add item to newChildren Array if its filtered
      if(!(dat.hasOwnProperty('droplab_hidden') && dat.droplab_hidden)){
        template.content.firstChild.style.display = 'block'
        newChildren.push(template.content.firstChild.outerHTML);
      }
    });
    //changelog: Add No result found if there is no result
    if(newChildren.length == 0){
      newChildren.push( '<li class="noresult">no result found</li>' );
    }
    toAppend = this.list.querySelector('ul[data-dynamic]');
    if(toAppend) {
      toAppend.innerHTML = newChildren.join('');
    } else {
      this.list.innerHTML = newChildren.join('');
    }
  },

  show: function() {
    this.list.style.display = 'block';
    this.hidden = false;
  },

  hide: function() {
    this.list.style.display = 'none';
    this.hidden = true;
  },
});

module.exports = DropDown;
