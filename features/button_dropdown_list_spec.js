/* eslint-env node, mocha */
/* global loadPage, winEval, expect */
require('./feature_helper');

describe('dynamically setting data', function() {

  loadPage('/features/button_dropdown_list.html');

  beforeEach(function() {
    this.example = function () {
      this.winEval(function (w) {
        w.droplab.addData('main', [{
          id: 24601,
          text: 'Jacob',
        }, {
          id: 24602,
          text: 'Jeff',
        }]);
      });
    };
  });

  it('sets the list items in the page', function () {
    this.example();
    var content = this.$('ul[data-dynamic]').innerHTML;
    expect(content).to.equal('<li style="display: block;"><a href="#" data-id="24601">Jacob</a></li><li style="display: block;"><a href="#" data-id="24602">Jeff</a></li>');
  });

  it('can set data multiple times', function () {
    this.example();
    this.winEval(function (w) {
      w.droplab.addData('main', [{
        id: 24603,
        text: 'Johan',
      }, {
        id: 24604,
        text: 'Justin',
      }]);
    });
    var content = this.$('ul[data-dynamic]').innerHTML;
    expect(content).to.equal('<li style="display: block;"><a href="#" data-id="24601">Jacob</a></li><li style="display: block;"><a href="#" data-id="24602">Jeff</a></li><li style="display: block;"><a href="#" data-id="24603">Johan</a></li><li style="display: block;"><a href="#" data-id="24604">Justin</a></li>');
  });

  it('shows the menu when the button is clicked', function() {
    this.example();

    var link = this.$('a[data-dropdown-trigger="#dropdown-1"]');
    var list = this.$('#dropdown-1');
    this.trigger(link, 'click', {tagName: 'A'});

    expect(list.style.display).to.equal('block');
  });

  it('hides the menu when something else is clicked', function() {
    this.example();

    var link = this.$('a[data-dropdown-trigger="#dropdown-1"]');
    var list = this.$('#dropdown-1');
    this.trigger(link, 'click', {tagName: 'A'});
    this.trigger(this.document.body, 'click', {tagName: 'BODY'});
    expect(list.style.display).to.equal('none');
  });

  it('hides the menu when an item is clicked', function() {
    this.example();

    var link = this.$('a[data-dropdown-trigger="#dropdown-1"]');
    var list = this.$('#dropdown-1');
    this.trigger(link, 'click', {tagName: 'A'});
    this.trigger(this.$('a[data-id]', list), 'click', {tagName: 'BODY'});
    expect(list.style.display).to.equal('none');
  });

  it('triggers an event on the button when an item is clicked', function () {
    return new Promise(function(resolve, reject) {
      this.example();

      var link = this.$('a[data-dropdown-trigger="#dropdown-1"]');
      var list = this.$('#dropdown-1');

      link.addEventListener('click.dl', function(e) {
        try {
          expect(e.detail).to.have.key('hook');
          resolve();
        } catch (err) {
          reject(err);
        }
      });
      this.trigger(link, 'click', {tagName: 'A'});
      this.trigger(this.$('a[data-id]', list), 'click', {tagName: 'BODY'});
    }.bind(this));
  });

  it('triggers an event on the list when an item is clicked', function () {
    return new Promise(function(resolve, reject) {
      this.example();

      var link = this.$('a[data-dropdown-trigger="#dropdown-1"]');
      var list = this.$('#dropdown-1');

      list.addEventListener('click.dl', function(e) {
        try {
          expect(e.detail).to.have.keys(['list', 'selected', 'data']);
          resolve();
        } catch (err) {
          reject(err);
        }
      });
      this.trigger(link, 'click', {tagName: 'A'});
      this.trigger(this.$('a[data-id]', list), 'click', {tagName: 'BODY'});
    }.bind(this));
  });

  it('should be no result found on empty data', function () {
    this.winEval(function (w) {
      w.droplab.addData('main', []);
    });
    var content = this.$('ul[data-dynamic]').innerHTML;
    expect(content).to.equal('<li class="noresult">no result found</li>');
  });
  
});
